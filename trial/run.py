from dim1 import Generator,Discriminator
from keras.layers import Input
from keras.models import Model
from util import DataDistribution, GeneratorDistribution
import numpy as np
import matplotlib.pyplot as plt

g = Generator()
d = Discriminator()

d.model.compile(optimizer='adam',
loss='binary_crossentropy',metrics=['accuracy'])

input_noise = Input(shape=(1,))
output_g = g.model(input_noise)
output_d_noise = d.model(output_g)

d.model.trainable = False
gd_model = Model(inputs=input_noise, outputs=output_d_noise)
gd_model.compile(optimizer='adam',loss='binary_crossentropy',
metrics=['accuracy'])


samples = DataDistribution([1,9]).sample(500)
samples = (samples-np.mean(samples))/(np.max(samples)-np.min(samples))
g_sampler = GeneratorDistribution(0.1)
batch_size = 16

#%%
vals = []
for i in range(10000):
    half_batch_size = int(batch_size/2)

    d.model.trainable = True
    chosen = np.random.choice(samples,half_batch_size)
    d.model.train_on_batch(chosen[:,None],np.ones(half_batch_size))

    noise = g_sampler.sample(half_batch_size)
    fake = g.model.predict_on_batch(noise)
    d.model.train_on_batch(fake,np.zeros(half_batch_size))

    # generator -----------
    d.model.trainable = False
    noise = g_sampler.sample(batch_size)
    gd_model.train_on_batch(noise[:,None],np.ones(batch_size))

    vals.append(np.mean(g.model.predict_on_batch(noise[:,None])))

#%%
pvals = g.model.predict_on_batch(g_sampler.sample(100))
d.model.predict_on_batch(np.linspace(-0.5,0.5,0.1))
gd_model.predict_on_batch(noise[:,None])
g.model.predict_on_batch(noise[:,None])

plt.hist(samples,bins=50,alpha=0.3)
plt.hist(g.model.predict_on_batch(g_sampler.sample(1000)),bins=50,alpha=0.3)
plt.figure()
plt.plot(vals)
