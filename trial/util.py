
import numpy as np

class DataDistribution(object):
    def __init__(self,mus=[6]):
        self.mus = mus
        self.sigma = 0.5

    def sample(self, N):
        samples  = []
        for mu in self.mus:
            samples += np.random.normal(mu, self.sigma, N).tolist()
        return np.array(sorted(samples))

class GeneratorDistribution(object):
    def __init__(self, range):
        self.range = range

    def sample(self, N):
        return np.linspace(-self.range, self.range, N) + \
            np.random.random(N) * 0.01
