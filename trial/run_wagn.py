from keras.layers import Input
from keras.models import Model
from keras.optimizers import Adam,RMSprop
from util import DataDistribution, GeneratorDistribution
from dim1_wgan import Generator,Discriminator,wloss
import numpy as np
import matplotlib.pyplot as plt

lr = 5e-5
clip = 0.01
n_critic = 5
batch_size = 32

g_model = Generator()
d_model = Discriminator(clip=clip)

adam = Adam(lr=lr)

d_model.compile(optimizer=adam,loss=wloss)

input_noise = Input(shape=(1,))
output_g = g_model(input_noise)
output_d_noise = d_model(output_g)

d_model.trainable = False
gd_model = Model(inputs=input_noise, outputs=output_d_noise)
gd_model.compile(optimizer=adam,loss=wloss)


samples = DataDistribution([-5,5]).sample(500)
#samples2 = DataDistribution([5]).sample(500)

#samples = (samples-np.mean(samples))/(np.max(samples)-np.min(samples))
g_sampler = GeneratorDistribution(0.1)



#%%
vals = []
losses = []
for i in range(10000):
#    half_batch_size = int(batch_size/2)

    d_model.trainable = True
    for _ in range(n_critic):
        chosen = np.random.choice(samples,batch_size)
        noise = g_sampler.sample(batch_size)
        fake = g_model.predict_on_batch(noise)
        input_data = np.concatenate([chosen,fake[:,0]])
        labels = np.concatenate([np.ones(batch_size),
        np.zeros(batch_size)])
        d_model.train_on_batch(input_data[:,None],labels)

    # generator train-----------
    d_model.trainable = False
    noise = g_sampler.sample(batch_size)
    gd_model.train_on_batch(noise[:,None],np.ones(batch_size))
    
    loss = d_model.evaluate(input_data,labels,verbose=0)
    losses.append(loss)
#    vals.append(np.mean(g_model.predict_on_batch(noise[:,None])))

plt.hist(samples,bins=50,alpha=0.3)
plt.hist(g_model.predict_on_batch(g_sampler.sample(1000)),bins=50,alpha=0.3)
#plt.figure()
#plt.plot(vals)
#plt.figure()
#plt.plot(np.linspace(-10,10,50),d_model.predict_on_batch(np.linspace(-10,10,50)))
plt.figure()
plt.plot([-x for x in losses])


#%%

for i in range(2000):
#    half_batch_size = int(batch_size/2)

    d_model.trainable = True
    for _ in range(n_critic):
        chosen = np.random.choice(samples,batch_size)
        chosen2 = np.random.choice(samples2,batch_size)
        input_data = np.concatenate([chosen,chosen2])
        labels = np.concatenate([np.ones(batch_size),
        np.zeros(batch_size)])
        d_model.train_on_batch(input_data[:,None],labels)

    
plt.hist(samples,bins=50,alpha=0.3)
plt.hist(samples2,bins=50,alpha=0.3)
#plt.hist(g_model.predict_on_batch(g_sampler.sample(1000)),bins=50,alpha=0.3)
#plt.figure()
#plt.plot(vals)
plt.plot(np.linspace(-10,10,50),d_model.predict_on_batch(np.linspace(-10,10,50)))
#%%
pvals = g_model.predict_on_batch(g_sampler.sample(100))
d_model.predict_on_batch(np.linspace(0,10,20))
gd_model.predict_on_batch(noise[:,None])
g_model.predict_on_batch(noise[:,None])


