from keras.layers import Input, Dense,LeakyReLU
from keras.models import Model,Sequential

class Generator(object):
    def __init__(self):
        alpha = 0.2
        self.model = Sequential([
            Dense(20, input_shape=(1,), name='gen1'),
            LeakyReLU(alpha=alpha),
            Dense(20, name='gen2'),
            LeakyReLU(alpha=alpha),
            Dense(1, activation='linear', name='gen3'),
        ])
        
class Discriminator(Generator):
    def __init__(self):
        alpha = 0.2
        self.model = Sequential([
            Dense(20, input_shape=(1,), name='d1'),
            LeakyReLU(alpha=alpha),
            Dense(20, name='d2'),
            LeakyReLU(alpha=alpha),
            Dense(1, activation='sigmoid', name='d3'),
        ])
