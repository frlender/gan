from keras.layers import Input
from keras.models import Model
from keras.optimizers import Adam,RMSprop
from util import get_data, noise_prior_factory, plot_pca
from dim1_wgan import Generator,Discriminator,wloss
import numpy as np
import matplotlib.pyplot as plt

lr = 5e-5
clip = 0.01
n_critic = 5
batch_size = 32
noise_dim = 100

train = get_data()
mat = train.values.T
g_sample = noise_prior_factory(train.max().max())

g_model = Generator()
d_model = Discriminator(clip=clip)

adam = Adam(lr=lr)

d_model.compile(optimizer=adam,loss=wloss)

input_noise = Input(shape=(noise_dim,))
output_g = g_model(input_noise)
output_d_noise = d_model(output_g)

d_model.trainable = False
gd_model = Model(inputs=input_noise, outputs=output_d_noise)
gd_model.compile(optimizer=adam,loss=wloss)

#%%
vals = []
losses = []
for i in range(200):
#    half_batch_size = int(batch_size/2)
    d_model.trainable = True
    for _ in range(n_critic):
        chosen = np.random.choice(mat,batch_size)
        noise = g_sample(batch_size,noise_dim)
        fake = g_model.predict_on_batch(noise)
        input_data = np.vstack([chosen,fake])
        labels = np.concatenate([np.ones(batch_size),
        np.zeros(batch_size)])
        d_model.train_on_batch(input_data[:,None],labels)

    # generator train-----------
    d_model.trainable = False
    noise = g_sample(batch_size,noise_dim)
    gd_model.train_on_batch(noise[:,None],np.ones(batch_size))

    loss = d_model.evaluate(input_data,labels,verbose=0)
    losses.append(loss)

plt.figure()
plt.plot([-x for x in losses])

noise = g_sample(500,noise_dim)
fake = g_model.predict_on_batch(noise)
plot_pca(mat,fake)
