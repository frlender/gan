import qn
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

def get_data():
    path = '/Users/qiaonan/Documents/single-cell-seq/scRNAseq-WGAN-GP/data/train.pkl'
    train = qn.load(path)
    return train

def noise_prior_factory(data_max_value):
    def noise_prior(batch_size, dim):
        temp_norm = np.random.normal(0.0, data_max_value/10, size=(batch_size, dim))
        temp_poisson = np.random.poisson(1, size=(batch_size, dim))
        return np.abs(temp_norm + temp_poisson)
    return noise_prior


def plot_pca(real,fake):
    mat = np.vstack([real,fake])
    real_labels = np.ones(np.shape(real)[0])
    fake_labels = np.zeros(np.shape(fake)[0])
    labels = np.concatenate([real_labels,fake_labels])
    pca = PCA(n_components=2)
    emb = pca.fit_transform(mat)
    plt.figure()
    plt.scatter(emb[:,0],emb[:,1],c=labels))
