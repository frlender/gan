from keras.layers import Input, Dense,LeakyReLU
from keras.models import Model,Sequential
from keras import backend as K
from keras.constraints import Constraint
from keras.initializers import RandomUniform
from keras.layers import BatchNormalization

def wloss(y_true,y_pred):
    # y_true are binary labels for real and fake data
    # minimize the negative
    scaled_true = 2.*(y_true-0.5) # 0，1 to -1, 1
    return -K.mean(scaled_true*y_pred)

class WeightClip(Constraint):
    '''Clips the weights incident to each hidden unit to be inside a range
    '''
    def __init__(self, c=2):
        self.c = c

    def __call__(self, p):
        return K.clip(p, -self.c, self.c)

    def get_config(self):
        return {'name': self.__class__.__name__,
                'c': self.c}

def Generator(input_dim=100,alpha=0.2):
    model = Sequential([
        Dense(600, input_shape=(input_dim,), name='gen1'),
        LeakyReLU(alpha=alpha),
        BatchNormalization(),
        Dense(600, name='gen2'),
        LeakyReLU(alpha=alpha),
        BatchNormalization(),
        Dense(6605, activation='relu', name='gen3')
    ])
    return model

def Discriminator(input_dim=6605,clip=0.01,alpha=0.2):
    randu = RandomUniform(minval=-clip,maxval=clip)
    wclip = WeightClip(clip)
    model = Sequential([
        Dense(200,
            input_shape=(input_dim,),
            kernel_constraint=wclip,
            kernel_initializer=randu,
            name='d1'),
        LeakyReLU(alpha=alpha),
        Dense(200,
            kernel_constraint=wclip,
            kernel_initializer=randu,
            name='d2'),
        LeakyReLU(alpha=alpha),
        Dense(1,
            activation='linear',
            kernel_constraint=wclip,
            kernel_initializer=randu,
            name='d3')
    ])
    return model
